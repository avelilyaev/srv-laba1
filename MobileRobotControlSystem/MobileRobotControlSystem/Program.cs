﻿using System;
using System.Windows.Forms;

namespace MobileRobotControlSystem
{
	static class Program
	{
		/// <summary>
		/// Объект формы
		/// </summary>
		public static MainForm ObjectMainForm { get; set; }

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			ObjectMainForm = new MainForm();
			Application.Run(ObjectMainForm);
		}
	}
}
