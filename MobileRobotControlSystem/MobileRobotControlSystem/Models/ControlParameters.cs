﻿using System;

namespace MobileRobotControlSystem.Models
{
	/// <summary>
	/// Класс, описывающий управляющие воздействия
	/// </summary>
	public static class ControlParameters
	{
		/// <summary>
		/// Координата X задаваемого вектора тяги робота
		/// Возможные значения: -100 - 100
		/// </summary>
		public static Double FRX { get; set; }

		/// <summary>
		/// Координата Y задаваемого вектора тяги робота
		/// Возможные значения: -100 - 100
		/// </summary>
		public static Double FRY { get; set; }

		/// <summary>
		/// Сформированное сообщение для отправки на сервер
		/// </summary>
		public static String Message
		{
			get { return "{(FRX=" + Convert.ToString(FRX).Replace(",", ".") + ")(FRY=" + Convert.ToString(FRY).Replace(",", ".") + ")}"; }
		}
	}
}
