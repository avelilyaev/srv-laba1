﻿using System;

namespace MobileRobotControlSystem.Models
{
	/// <summary>
	/// Класс описывающий параметры, пришедшие с сервера
	/// </summary>
	public static class Parameters
	{
		/// <summary>
		/// Время моделирования в секундах
		/// Возможные значения: >= 0
		/// </summary>
		public static Double T { get; set; }

		/// <summary>
		/// Радиус робота
		/// Возможные значения: 0.01 - 0.07
		/// </summary>
		public static Double R { get; set; }

		/// <summary>
		/// Текущая масса робота
		/// Возможные значения: 0.01 - 100
		/// </summary>
		public static Double M { get; set; }

		/// <summary>
		/// Координата X центра робота
		/// Возможные значения: -1 - 1
		/// </summary>
		public static Double RX { get; set; }

		/// <summary>
		/// Координата Y центра робота
		/// Возможные значения: -1 - 1
		/// </summary>
		public static Double RY { get; set; }

		/// <summary>
		/// Координата X вектора скорости робота
		/// Возможные значения: -100 - 100
		/// </summary>
		public static Double VRX { get; set; }

		/// <summary>
		/// Координата Y вектора скорости робота
		/// Возможные значения: -100 - 100
		/// </summary>
		public static Double VRY { get; set; }

		/// <summary>
		/// Радиус цели для третьей задачи управления (при TASK=3)
		/// Возможные значения: 0.1 - 0.2
		/// </summary>
		public static Double TR { get; set; }

		/// <summary>
		/// Координата X центра цели для третьей задачи управления (при TASK=3)
		/// Возможные значения: -1 - 1
		/// </summary>
		public static Double TX { get; set; }

		/// <summary>
		/// Координата Y центра цели для третьей задачи управления (при TASK=3)
		/// Возможные значения: -1 - 1
		/// </summary>
		public static Double TY { get; set; }

		/// <summary>
		/// Номер текущей задачи управления (1 - идти на точкуб 2 - идти по маршруту, 3 - преследовать цель)
		/// Возможные значения: 1, 2, 3
		/// </summary>
		public static Int32 TASK { get; set; }

		/// <summary>
		/// Номер текущей засчитанной точки маршрута для задачи управления 2 - идти по маршруту
		/// Возможные значения: 0, 1, 2, 3, 4, 5, 6
		/// </summary>
		public static Int32 POINT { get; set; }

		/// <summary>
		/// Состояние процесса управления. 0 - норма, 1 - нарушены условия или ограничения на текущую решаемую задачу управления
		/// Возможные значения: 0, 1
		/// </summary>
		public static Int32 FAIL { get; set; }

		/// <summary>
		/// Состояние робота. 0 - норма. 1 - робот разрушен при касании стенки
		/// Возможные значения: 0, 1
		/// </summary>
		public static Int32 CRASH { get; set; }

		/// <summary>
		/// Запас топлива (отбрасываемой массы) в килограммах. Если = 0 - топливо кончилось.
		/// Возможные значения: >= 0
		/// </summary>
		public static Double FUEL { get; set; }

		/// <summary>
		/// Статус моделирования. 0 - работаем. 1 - все три задачи управления выполнены, моделирование завершено
		/// Возможные значения: 0, 1
		/// </summary>
		public static Int32 DONE { get; set; }

		/// <summary>
		/// Счетчик управляющих сообщений в неверном формате 
		/// Возможные значения: >= 0
		/// </summary>
		public static Int32 ERROR { get; set; }

		/// <summary>
		/// Счетчик игнорированных управляющих сообщений (по формату или по времени)
		/// Возможные значения: >= 0
		/// </summary>
		public static Int32 IGNORED { get; set; }

		/// <summary>
		/// Множитель для отображения вектора скорости перемещения
		/// </summary>
		public static Int32 SpeedMultiplier { get { return 400; } }

		/// <summary>
		/// Множитель для корректного отображения координат
		/// </summary>
		public static Int32 CoordinatesMultiplier { get { return 200; } }
	}
}