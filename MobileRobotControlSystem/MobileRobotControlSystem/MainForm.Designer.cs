﻿using System.Drawing;
using MobileRobotControlSystem.Models;
namespace MobileRobotControlSystem
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
			System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.T = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.R = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.M = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.RX = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.RY = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.VRX = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.VRY = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TR = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TX = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TY = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TASK = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.POINT = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.CRASH = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FUEL = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ERROR = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.IGNORED = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.startButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.exitButton = new System.Windows.Forms.Button();
			this.stopButton = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.applyMultiplierButton = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.controlGridView = new System.Windows.Forms.DataGridView();
			this.FRX = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FRY = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label5 = new System.Windows.Forms.Label();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.timer2 = new System.Windows.Forms.Timer(this.components);
			this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.label6 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.controlGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView1
			// 
			this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
			this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ScrollBar;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.T,
            this.R,
            this.M,
            this.RX,
            this.RY,
            this.VRX,
            this.VRY,
            this.TR,
            this.TX,
            this.TY,
            this.TASK,
            this.POINT,
            this.FAIL,
            this.CRASH,
            this.FUEL,
            this.DONE,
            this.ERROR,
            this.IGNORED});
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
			this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
			this.dataGridView1.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.dataGridView1.Location = new System.Drawing.Point(5, 19);
			this.dataGridView1.MultiSelect = false;
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.dataGridView1.RowHeadersVisible = false;
			this.dataGridView1.RowTemplate.ReadOnly = true;
			this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.dataGridView1.Size = new System.Drawing.Size(1064, 45);
			this.dataGridView1.TabIndex = 0;
			// 
			// T
			// 
			this.T.HeaderText = "T";
			this.T.Name = "T";
			this.T.ReadOnly = true;
			this.T.Width = 50;
			// 
			// R
			// 
			this.R.HeaderText = "R";
			this.R.Name = "R";
			this.R.ReadOnly = true;
			this.R.Width = 60;
			// 
			// M
			// 
			this.M.HeaderText = "M";
			this.M.Name = "M";
			this.M.ReadOnly = true;
			this.M.Width = 70;
			// 
			// RX
			// 
			this.RX.HeaderText = "RX";
			this.RX.Name = "RX";
			this.RX.ReadOnly = true;
			this.RX.Width = 60;
			// 
			// RY
			// 
			this.RY.HeaderText = "RY";
			this.RY.Name = "RY";
			this.RY.ReadOnly = true;
			this.RY.Width = 60;
			// 
			// VRX
			// 
			this.VRX.HeaderText = "VRX";
			this.VRX.Name = "VRX";
			this.VRX.ReadOnly = true;
			this.VRX.Width = 70;
			// 
			// VRY
			// 
			this.VRY.HeaderText = "VRY";
			this.VRY.Name = "VRY";
			this.VRY.ReadOnly = true;
			this.VRY.Width = 70;
			// 
			// TR
			// 
			this.TR.HeaderText = "TR";
			this.TR.Name = "TR";
			this.TR.ReadOnly = true;
			this.TR.Width = 50;
			// 
			// TX
			// 
			this.TX.HeaderText = "TX";
			this.TX.Name = "TX";
			this.TX.ReadOnly = true;
			this.TX.Width = 50;
			// 
			// TY
			// 
			this.TY.HeaderText = "TY";
			this.TY.Name = "TY";
			this.TY.ReadOnly = true;
			this.TY.Width = 50;
			// 
			// TASK
			// 
			this.TASK.HeaderText = "TASK";
			this.TASK.Name = "TASK";
			this.TASK.ReadOnly = true;
			this.TASK.Width = 50;
			// 
			// POINT
			// 
			this.POINT.HeaderText = "POINT";
			this.POINT.Name = "POINT";
			this.POINT.ReadOnly = true;
			this.POINT.Width = 50;
			// 
			// FAIL
			// 
			this.FAIL.HeaderText = "FAIL";
			this.FAIL.Name = "FAIL";
			this.FAIL.ReadOnly = true;
			this.FAIL.Width = 50;
			// 
			// CRASH
			// 
			this.CRASH.HeaderText = "CRASH";
			this.CRASH.Name = "CRASH";
			this.CRASH.ReadOnly = true;
			this.CRASH.Width = 50;
			// 
			// FUEL
			// 
			this.FUEL.HeaderText = "FUEL";
			this.FUEL.Name = "FUEL";
			this.FUEL.ReadOnly = true;
			this.FUEL.Width = 70;
			// 
			// DONE
			// 
			this.DONE.HeaderText = "DONE";
			this.DONE.Name = "DONE";
			this.DONE.ReadOnly = true;
			this.DONE.Width = 50;
			// 
			// ERROR
			// 
			this.ERROR.HeaderText = "ERROR";
			this.ERROR.Name = "ERROR";
			this.ERROR.ReadOnly = true;
			this.ERROR.Width = 50;
			// 
			// IGNORED
			// 
			this.IGNORED.HeaderText = "IGNORED";
			this.IGNORED.Name = "IGNORED";
			this.IGNORED.ReadOnly = true;
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(135, 488);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(128, 40);
			this.startButton.TabIndex = 2;
			this.startButton.Text = "Начать моделирование";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.startButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 501);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(114, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Масштаб: 1 px = 1 см";
			// 
			// exitButton
			// 
			this.exitButton.Location = new System.Drawing.Point(943, 488);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(128, 40);
			this.exitButton.TabIndex = 4;
			this.exitButton.Text = "Выйти";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// stopButton
			// 
			this.stopButton.Enabled = false;
			this.stopButton.Location = new System.Drawing.Point(278, 488);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(128, 40);
			this.stopButton.TabIndex = 5;
			this.stopButton.Text = "Остановить моделирование";
			this.stopButton.UseVisualStyleBackColor = true;
			this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(805, 65);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(259, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Таблица 1. Параметры робота в текущий момент";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(3, 3);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(275, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "Лабораторная работа №1. По курсу ПО СРВ.";
			// 
			// timer1
			// 
			this.timer1.Interval = 50;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// applyMultiplierButton
			// 
			this.applyMultiplierButton.Location = new System.Drawing.Point(406, 287);
			this.applyMultiplierButton.Name = "applyMultiplierButton";
			this.applyMultiplierButton.Size = new System.Drawing.Size(206, 40);
			this.applyMultiplierButton.TabIndex = 8;
			this.applyMultiplierButton.Text = "Применить коэффициент управления";
			this.applyMultiplierButton.UseVisualStyleBackColor = true;
			this.applyMultiplierButton.Click += new System.EventHandler(this.applyMultiplierButton_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(508, 261);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(104, 20);
			this.textBox1.TabIndex = 9;
			this.textBox1.Text = "0,1";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(416, 265);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(80, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Коэффициент:";
			// 
			// controlGridView
			// 
			this.controlGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.controlGridView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			this.controlGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Control;
			dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this.controlGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this.controlGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.controlGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FRX,
            this.FRY});
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.controlGridView.DefaultCellStyle = dataGridViewCellStyle5;
			this.controlGridView.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.controlGridView.Location = new System.Drawing.Point(406, 194);
			this.controlGridView.Name = "controlGridView";
			this.controlGridView.RowHeadersVisible = false;
			this.controlGridView.Size = new System.Drawing.Size(204, 42);
			this.controlGridView.TabIndex = 12;
			// 
			// FRX
			// 
			this.FRX.HeaderText = "FRX";
			this.FRX.Name = "FRX";
			this.FRX.ReadOnly = true;
			// 
			// FRY
			// 
			this.FRY.HeaderText = "FRY";
			this.FRY.Name = "FRY";
			this.FRY.ReadOnly = true;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(409, 237);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(206, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "Таблица 2. Управляющие воздействия";
			// 
			// pictureBox2
			// 
			this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox2.Image = global::MobileRobotControlSystem.Properties.Resources.legend;
			this.pictureBox2.Location = new System.Drawing.Point(408, 86);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(204, 106);
			this.pictureBox2.TabIndex = 14;
			this.pictureBox2.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
			this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBox1.Location = new System.Drawing.Point(5, 86);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(400, 400);
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// timer2
			// 
			this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
			// 
			// chart1
			// 
			this.chart1.BorderlineColor = System.Drawing.Color.Black;
			this.chart1.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
			chartArea1.Name = "ChartArea1";
			this.chart1.ChartAreas.Add(chartArea1);
			legend1.Name = "Legend1";
			this.chart1.Legends.Add(legend1);
			this.chart1.Location = new System.Drawing.Point(615, 86);
			this.chart1.Name = "chart1";
			series1.ChartArea = "ChartArea1";
			series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series1.Legend = "Legend1";
			series1.Name = "X";
			series2.ChartArea = "ChartArea1";
			series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
			series2.Legend = "Legend1";
			series2.Name = "Y";
			this.chart1.Series.Add(series1);
			this.chart1.Series.Add(series2);
			this.chart1.Size = new System.Drawing.Size(454, 400);
			this.chart1.TabIndex = 15;
			this.chart1.Text = "chart1";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label6.Location = new System.Drawing.Point(618, 489);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(311, 13);
			this.label6.TabIndex = 16;
			this.label6.Text = "График 1. Разгонные переходные характеристики X(t) и Y(t)";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(1071, 528);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.chart1);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.controlGridView);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.applyMultiplierButton);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.stopButton);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.startButton);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.dataGridView1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Система управления мобильным роботом";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.controlGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#region my methods
		public MainForm() { InitializeComponent(); }
		/// <summary>
		/// Bind Grid and Parameters
		/// </summary>
		public void BindGridAndParameters()
		{
			this.dataGridView1[0, 0].Value = Parameters.T;
			this.dataGridView1[1, 0].Value = Parameters.R;
			this.dataGridView1[2, 0].Value = Parameters.M;
			this.dataGridView1[3, 0].Value = Parameters.RX;
			this.dataGridView1[4, 0].Value = Parameters.RY;
			this.dataGridView1[5, 0].Value = Parameters.VRX;
			this.dataGridView1[6, 0].Value = Parameters.VRY;
			this.dataGridView1[7, 0].Value = Parameters.TR;
			this.dataGridView1[8, 0].Value = Parameters.TX;
			this.dataGridView1[9, 0].Value = Parameters.TY;
			this.dataGridView1[10, 0].Value = Parameters.TASK;
			this.dataGridView1[11, 0].Value = Parameters.POINT;
			this.dataGridView1[12, 0].Value = Parameters.FAIL;
			this.dataGridView1[13, 0].Value = Parameters.CRASH;
			this.dataGridView1[14, 0].Value = Parameters.FUEL;
			this.dataGridView1[15, 0].Value = Parameters.DONE;
			this.dataGridView1[16, 0].Value = Parameters.ERROR;
			this.dataGridView1[17, 0].Value = Parameters.IGNORED;
		}

		/// <summary>
		/// Bind Second grid and control parameters
		/// </summary>
		public void BindSecondGrid()
		{
			this.controlGridView[0, 0].Value = ControlParameters.FRX;
			this.controlGridView[1, 0].Value = ControlParameters.FRY;
		}
		#endregion

		#endregion

		public System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button startButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.Button stopButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewTextBoxColumn T;
		private System.Windows.Forms.DataGridViewTextBoxColumn R;
		private System.Windows.Forms.DataGridViewTextBoxColumn M;
		private System.Windows.Forms.DataGridViewTextBoxColumn RX;
		private System.Windows.Forms.DataGridViewTextBoxColumn RY;
		private System.Windows.Forms.DataGridViewTextBoxColumn VRX;
		private System.Windows.Forms.DataGridViewTextBoxColumn VRY;
		private System.Windows.Forms.DataGridViewTextBoxColumn TR;
		private System.Windows.Forms.DataGridViewTextBoxColumn TX;
		private System.Windows.Forms.DataGridViewTextBoxColumn TY;
		private System.Windows.Forms.DataGridViewTextBoxColumn TASK;
		private System.Windows.Forms.DataGridViewTextBoxColumn POINT;
		private System.Windows.Forms.DataGridViewTextBoxColumn FAIL;
		private System.Windows.Forms.DataGridViewTextBoxColumn CRASH;
		private System.Windows.Forms.DataGridViewTextBoxColumn FUEL;
		private System.Windows.Forms.DataGridViewTextBoxColumn DONE;
		private System.Windows.Forms.DataGridViewTextBoxColumn ERROR;
		private System.Windows.Forms.DataGridViewTextBoxColumn IGNORED;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button applyMultiplierButton;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.DataGridViewTextBoxColumn FRX;
		private System.Windows.Forms.DataGridViewTextBoxColumn FRY;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.PictureBox pictureBox2;
		public System.Windows.Forms.DataGridView controlGridView;
		private System.Windows.Forms.Timer timer2;
		private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
		private System.Windows.Forms.Label label6;


	}
}

