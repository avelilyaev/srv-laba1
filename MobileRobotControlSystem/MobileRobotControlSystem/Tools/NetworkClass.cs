﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using MobileRobotControlSystem.Models;

namespace MobileRobotControlSystem.Tools
{
	/// <summary>
	/// Класс межсетевого взаимодействия
	/// </summary>
	public static class NetworkClass
	{
		/// <summary>
		/// Получить сообщение с сервера
		/// </summary>
		public static void GetParameters()
		{
			byte[] bytes = new byte[512];
			int lengthMessage = 0;
			try { lengthMessage = Socket.Receive(bytes); }
			catch (Exception e) { }
			if (lengthMessage > 0)
				BindWithParametes(GetWholeMessage(Encoding.UTF8.GetString(bytes, 0, lengthMessage)));
		}

		/// <summary>
		/// Отправить сообщение на сервер
		/// </summary>
		public static void SendParameters()
		{
			byte[] msg = Encoding.UTF8.GetBytes(ControlParameters.Message);
			Socket.Send(msg);
		}

		/// <summary>
		/// Подключение к серверу
		/// </summary>
		public static void ConnectToServer()
		{
			Socket = new Socket(ServerAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			Socket.Connect(IpEndPoint);
		}

		/// <summary>
		/// Реконнект к серверу
		/// </summary>
		public static void Disconnect()
		{
			Socket.Disconnect(false);
		}

		/// <summary>
		/// Вычленяем целое сообщение
		/// </summary>
		private static String GetWholeMessage(string text)
		{
			String newText = string.Empty;
			for (int i = 0; i < text.Length; i++)
			{
				if (text[i] == '.')
				{
					newText += ',';
				}
				else
					newText += text[i];
			}
			char[] separators = { '{', '}' };
			var array = newText.Split(separators).Where(x => x.Length > 0).ToArray();
			var max = array[0];
			foreach (var item in array)
			{
				max = item.Length > max.Length ? item : max;
			}
			return max;
		}

		/// <summary>
		/// Маппинг каждого свойства класса Parameters на колонку Таблицы 1
		/// </summary>
		private static void BindWithParametes(String message)
		{
			char[] separator = { '(', ')' };
			var array = message.Split(separator).Where(x => x.Length > 0).ToArray();
			foreach (var item in array)
			{
				var splitArray = item.Split('=');
				switch (splitArray[0])
				{
					case "T":
						Parameters.T = Convert.ToDouble(splitArray[1]);
						break;
					case "R":
						Parameters.R = Convert.ToDouble(splitArray[1]) * Parameters.CoordinatesMultiplier;
						break;
					case "M":
						Parameters.M = Convert.ToDouble(splitArray[1]);
						break;
					case "RX":
						Parameters.RX = Convert.ToDouble(splitArray[1]) * Parameters.CoordinatesMultiplier;
						break;
					case "RY":
						Parameters.RY = Convert.ToDouble(splitArray[1]) * Parameters.CoordinatesMultiplier;
						break;
					case "VRX":
						Parameters.VRX = Convert.ToDouble(splitArray[1]) * Parameters.SpeedMultiplier;
						break;
					case "VRY":
						Parameters.VRY = Convert.ToDouble(splitArray[1]) * Parameters.SpeedMultiplier;
						break;
					case "TR":
						Parameters.TR = Convert.ToDouble(splitArray[1]) * Parameters.CoordinatesMultiplier;
						break;
					case "TX":
						Parameters.TX = Convert.ToDouble(splitArray[1]) * Parameters.CoordinatesMultiplier;
						break;
					case "TY":
						Parameters.TY = Convert.ToDouble(splitArray[1]) * Parameters.CoordinatesMultiplier;
						break;
					case "TASK":
						Parameters.TASK = Convert.ToInt32(splitArray[1]);
						break;
					case "POINT":
						Parameters.POINT = Convert.ToInt32(splitArray[1]);
						break;
					case "FAIL":
						Parameters.FAIL = Convert.ToInt32(splitArray[1]);
						break;
					case "CRASH":
						Parameters.CRASH = Convert.ToInt32(splitArray[1]);
						break;
					case "FUEL":
						Parameters.FUEL = Convert.ToDouble(splitArray[1]);
						break;
					case "DONE":
						Parameters.DONE = Convert.ToInt32(splitArray[1]);
						break;
					case "ERROR":
						Parameters.ERROR = Convert.ToInt32(splitArray[1]);
						break;
					case "IGNORED":
						Parameters.IGNORED = Convert.ToInt32(splitArray[1]);
						break;
				}
			}

			//Отображаем полученные данные на гриде
			Program.ObjectMainForm.BindGridAndParameters();
		}

		/// <summary>
		/// IP - Адрес сервера. В нашем случае - loopback
		/// </summary>
		private static IPAddress ServerAddress { get { return IPAddress.Parse("127.0.0.1"); } }

		/// <summary>
		/// Конечная точка для подключения
		/// </summary>
		private static IPEndPoint IpEndPoint { get { return new IPEndPoint(ServerAddress, 9999); } }

		/// <summary>
		/// Сокет
		/// </summary>
		private static Socket Socket { get; set; }
	}
}
