﻿using System;
using System.Drawing;
using MobileRobotControlSystem.Models;

namespace MobileRobotControlSystem.Tools
{
	/// <summary>
	/// Класс графических операций
	/// </summary>
	public static class GraphicClass
	{
		/// <summary>
		/// Отображение робота и векторов
		/// </summary>
		public static void DrawShape()
		{
			//Отображаем робота
			Circle(Parameters.RX, Parameters.RY, Parameters.R, Color.Black);

			//Отображаем вектор скорости
			Line(Parameters.RX, Parameters.RY, Parameters.RX + Parameters.VRX, Parameters.RY + Parameters.VRY, Color.Blue);

			//Отображаем вектор тяги
			Line(Parameters.RX, Parameters.RY, Parameters.RX + ControlParameters.FRX * 200, Parameters.RY + ControlParameters.FRY * 200, Color.Red);
		}

		/// <summary>
		/// Линия Брезенхема
		/// </summary>
		public static void Line(Double x1, Double y1, Double x2, Double y2, Color clr)
		{
			bool steep = (Math.Abs(y2 - y1) > Math.Abs(x2 - x1));
			if (steep)
			{
				Double tmp;
				tmp = x1;
				x1 = y1;
				y1 = tmp;
				tmp = x2;
				x2 = y2;
				y2 = tmp;
			}

			if (x1 > x2)
			{
				Double tmp;
				tmp = x1;
				x1 = x2;
				x2 = tmp;

				tmp = y1;
				y1 = y2;
				y2 = tmp;
			}

			Double dx = Math.Abs(x2 - x1);
			Double dy = Math.Abs(y2 - y1);

			Double error = dx / 2.0f;
			int ystep = (y1 < y2) ? 1 : -1;
			Double y = y1;

			Double maxX = x2;

			for (Double x = x1; x <= maxX; x++)
			{
				if (steep)
				{
					_setPixel(Convert.ToInt32(y), Convert.ToInt32(x), clr);
				}
				else
				{
					_setPixel(Convert.ToInt32(x), Convert.ToInt32(y), clr);
				}

				error -= dy;
				if (error < 0)
				{
					y += ystep;
					error += dx;
				}
			}
		}

		/// <summary>
		/// Окружность Брезенхема
		/// </summary>
		public static void Circle(Double x1, Double y1, Double r, Color color)
		{
			Double x = 0;
			Double y = r;
			Double delta = 1 - 2 * r;
			Double error = 0;
			while (y >= 0)
			{
				_setPixel(Convert.ToInt32(x1 + x), Convert.ToInt32(y1 + y), color);
				_setPixel(Convert.ToInt32(x1 + x), Convert.ToInt32(y1 - y), color);
				_setPixel(Convert.ToInt32(x1 - x), Convert.ToInt32(y1 + y), color);
				_setPixel(Convert.ToInt32(x1 - x), Convert.ToInt32(y1 - y), color);
				error = 2 * (delta + y) - 1;
				if ((delta < 0) && (error <= 0))
				{
					delta += 2 * ++x + 1;
					continue;
				}
				error = 2 * (delta - x) - 1;
				if ((delta > 0) && (error > 0))
				{
					delta += 1 - 2 * --y;
					continue;
				}
				x++;
				delta += 2 * (x - y);
				y--;
			}
		}

		/// <summary>
		/// Отметить цель на заданных координатах
		/// </summary>
		public static void DrawTarget(Point point)
		{
			if (ControlTask.CurrentTask == ControlTaskNumber.Third)
			{
				Circle(point.X, point.Y, Parameters.TR, Color.Red);
			}
			else
			{
				Line(point.X - 1, point.Y, point.X + 1, point.Y, Color.Red);
				Line(point.X, point.Y - 1, point.X, point.Y + 1, Color.Red);
			}
		}

		/// <summary>
		/// Метод "SetPixel"
		/// Переводит координаты из однородной системы в декартову
		/// </summary>
		private static void _setPixel(int x, int y, Color clr)
		{
			var w = Program.ObjectMainForm.ImageBitmap.Width;
			var h = Program.ObjectMainForm.ImageBitmap.Height;
			//Защита от исключения выхода за границы холста
			if (x > -w / 2 && x < w / 2 && y < h / 2 && y > -h / 2)
			{
				Program.ObjectMainForm.ImageBitmap.SetPixel(w / 2 + x, h / 2 - y, clr);
				Program.ObjectMainForm.pictureBox1.Invalidate();
			}
		}
	}
}
