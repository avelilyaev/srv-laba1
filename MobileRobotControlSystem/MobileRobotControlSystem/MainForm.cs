﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using MobileRobotControlSystem.Models;
using MobileRobotControlSystem.Tools;

namespace MobileRobotControlSystem
{
	public partial class MainForm : Form
	{
		/// <summary>
		/// Холст, на котором происходит визуализация робота (400 на 400 сантиметров)
		/// </summary>
		public Bitmap ImageBitmap { get; set; }

		/// <summary>
		/// Процесс "Сервер"
		/// </summary>
		private Process ServerProcess { get; set; }

		/// <summary>
		/// Действия во время загрузки формы
		/// </summary>
		private void MainForm_Load(object sender, EventArgs e)
		{
			//Пытаемся найти запущенный процесс сервера. Если не нашли, то запускаем сами
			ServerProcess = Process.GetProcessesByName("ModelingServer").FirstOrDefault() ?? Process.Start("ModelingServer.exe");

			//Считываем коэффициент управляющего воздействия с формы
			ControlTask.Multiplier = Convert.ToDouble(textBox1.Text);
		}
		/// <summary>
		/// Уходя, гасим сервер
		/// </summary>
		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			//Если процесс сервера не закрыт, то закрываем его
			if (!ServerProcess.HasExited)
			{
				ServerProcess.CloseMainWindow();
			}
			ServerProcess.Close();
		}

		/// <summary>
		/// Обработчик события таймера на прием
		/// </summary>
		private void timer1_Tick(object sender, EventArgs e)
		{
			//Стираем старый кадр
			pictureBox1.Image = ImageBitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);

			//Получаем текущие параметры с сервера
			NetworkClass.GetParameters();

			//Производим управляющее воздействие
			ControlTask.MakeControlAction();

			//Отображаем разгонные переходные характеристики x(t) и y(t) соответственно
			chart1.Series["X"].Points.AddY(Parameters.RX);
			chart1.Series["Y"].Points.AddY(Parameters.RY);
		}

		/// <summary>
		/// Обработчик события таймера на передачу
		/// </summary>
		private void timer2_Tick(object sender, EventArgs e)
		{
			NetworkClass.SendParameters();
		}

		/// <summary>
		/// Обработчик нажатия кнопки "Начать моделирование"
		/// </summary>
		private void startButton_Click(object sender, EventArgs e)
		{
			//Подключаемся к серверу
			NetworkClass.ConnectToServer();

			//Начинаем моделирование
			timer1.Start();
			timer2.Start();
			//Устанавливаем состояние кнопок
			startButton.Enabled = false;
			stopButton.Enabled = true;

			ControlTask.RouteOfSecondTask = new List<Point>();
			ControlTask.RouteOfSecondTask.Add(new Point(140, 0));
			ControlTask.RouteOfSecondTask.Add(new Point(0, 140));
			ControlTask.RouteOfSecondTask.Add(new Point(-60, 0));
			ControlTask.RouteOfSecondTask.Add(new Point(-140, 0));
			ControlTask.RouteOfSecondTask.Add(new Point(140, 0));
			ControlTask.RouteOfSecondTask.Add(new Point(0, -160));

			ControlTask.CurrentPointNumber = 0;
		}

		/// <summary>
		/// Обработчик нажатия кнопки "Остановить моделирование"
		/// </summary>
		private void stopButton_Click(object sender, EventArgs e)
		{
			//Останавливаем моделирование
			timer1.Stop();
			timer2.Stop();

			//Очищаем грид и холст
			dataGridView1.Rows.Clear();
			pictureBox1.Image = ImageBitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);

			//Отключаеся от сервера
			NetworkClass.Disconnect();

			//Меняем доступность кнопок
			startButton.Enabled = true;
			stopButton.Enabled = false;

			//Очищаем графики
			chart1.Series["X"].Points.Clear();
			chart1.Series["Y"].Points.Clear();
		}

		/// <summary>
		/// Обработчик нажатия кнопки "Выйти"
		/// </summary>
		private void exitButton_Click(object sender, EventArgs e)
		{
			//Отключаеся от сервера
			NetworkClass.Disconnect();
			Close();
		}

		/// <summary>
		/// Обработчик нажатия кнопки "Применить коэффициент управления"
		/// </summary>
		private void applyMultiplierButton_Click(object sender, EventArgs e)
		{
			textBox1.Text = textBox1.Text.Replace('.', ',');
			ControlTask.Multiplier = Convert.ToDouble(textBox1.Text);
		}
	}
}