﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MobileRobotControlSystem.Models;
using MobileRobotControlSystem.Tools;

namespace MobileRobotControlSystem
{
	/// <summary>
	/// Класс, описывающий задачи и алгоритмы управления
	/// </summary>
	public static class ControlTask
	{
		public static Double PrevLength { get; set; }

		/// <summary>
		/// Номер текущей точки для второго задания
		/// </summary>
		public static Int32 CurrentPointNumber { get; set; }

		/// <summary>
		/// Маршрут второй задачи
		/// </summary>
		public static List<Point> RouteOfSecondTask { get; set; }

		/// <summary>
		/// Варьируемый коэффициент алгоритма управления
		/// </summary>
		public static Double Multiplier { get; set; }

		/// <summary>
		/// Текущая задача управления роботом
		/// </summary>
		public static ControlTaskNumber CurrentTask
		{
			get { return (ControlTaskNumber)Parameters.TASK - 1; }
		}

		/// <summary>
		/// Координаты текущей цели
		/// </summary>
		public static Point CurrentTargetsCoordinates { get; set; }
		//вернуть прайвет


		/// <summary>
		/// Произвести управляющее воздействие
		/// </summary>
		public static void MakeControlAction()
		{
			switch (CurrentTask)
			{
				case ControlTaskNumber.First:
					{
						//Устанавливаем цель в начале координат
						CurrentTargetsCoordinates = new Point(0, 0);

						//Отмечаем цель на холсте
						GraphicClass.DrawTarget(CurrentTargetsCoordinates);
					}
					break;
				case ControlTaskNumber.Second:
					{
						//Устанавливаем текущую цель
						CurrentTargetsCoordinates = RouteOfSecondTask.FirstOrDefault();

						//Отмечаем цели на холсте
						foreach (var target in RouteOfSecondTask)
						{
							GraphicClass.DrawTarget(target);
						}
					}
					break;
				case ControlTaskNumber.Third:
					{
						//Устанавливаем цель на движущийся объект
						CurrentTargetsCoordinates = new Point(Convert.ToInt32(Parameters.TX), Convert.ToInt32(Parameters.TY));

						//Отмечаем цель на холсте
						GraphicClass.DrawTarget(CurrentTargetsCoordinates);
					}
					break;
			}

			//Производим управлющее воздействие
			SetControlAction();

			//Отрисовываем робота
			GraphicClass.DrawShape();
		}

		/// <summary>
		/// Засчитать текущую точку
		/// </summary>
		public static void PassPoint()
		{
			if (RouteOfSecondTask.Count > 0 && CurrentTask == ControlTaskNumber.Second)
			{
				RouteOfSecondTask.Remove(RouteOfSecondTask[0]);
				CurrentTargetsCoordinates = RouteOfSecondTask.FirstOrDefault();
				CurrentPointNumber++;
			}
		}

		/// <summary>
		/// Произвести управляющее воздействие
		/// </summary>
		public static void SetControlAction()
		{
			double dx = CurrentTargetsCoordinates.X - Parameters.RX;
			double dy = CurrentTargetsCoordinates.Y - Parameters.RY;

			double l = Math.Sqrt(dx * dx + dy * dy);
			double lengthV = Math.Sqrt((Parameters.VRX - Parameters.RX) * (Parameters.VRX - Parameters.RX) + (Parameters.VRY - Parameters.RY) * (Parameters.VRY - Parameters.RY));


			if (CurrentTask != ControlTaskNumber.Third && ((Math.Abs(dx) < Parameters.R/3) && (Math.Abs(dy) < Parameters.R/3) || Parameters.POINT > CurrentPointNumber))
			{
				PassPoint();
				ControlParameters.FRX = Parameters.VRX / (100);
				ControlParameters.FRY = Parameters.VRY / (100);
			}
			else
			{
				if ((Math.Abs(dx) < Parameters.R * 2) && (Math.Abs(dy) < Parameters.R * 2) && PrevLength < l)
				{
					ControlParameters.FRX = Parameters.VRX / (100);
					ControlParameters.FRY = Parameters.VRY / (100);
				}
				else
				{
					ControlParameters.FRX = Multiplier * (-dx) / (100);
					ControlParameters.FRY = Multiplier * (-dy) / (100);
				}
			}
			
			Program.ObjectMainForm.BindSecondGrid();
			PrevLength = l;
		}
	}

	/// <summary>
	/// Перечисление задач упраавления роботом
	/// </summary>
	public enum ControlTaskNumber
	{
		/// <summary>
		/// Достижение заданной точки
		/// </summary>
		First,

		/// <summary>
		/// Следование по маршруту
		/// </summary>
		Second,

		/// <summary>
		/// Преследование перемещающегося объекта
		/// </summary>
		Third
	}
}
